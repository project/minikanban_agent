<?php

namespace Drupal\minikanban_agent\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Defines a local action plugin with a dynamic title.
 */
class AddSwarmAction extends LocalActionDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $route_parameters = parent::getRouteParameters($route_match);
    // It should redirect to the current page.
    $route_parameters['destination'] = \Drupal::destination()->getAsArray()['destination'] ?? '';
    $route_parameters['is_swarm'] = TRUE;
    return $route_parameters;
  }

}
