<?php

namespace Drupal\minikanban_agent\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a local action plugin with a dynamic title.
 */
class AddProjectToSwarmAction extends LocalActionDefault {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $route_parameters = parent::getRouteParameters($route_match);
    // It should redirect to the current page.
    $route_parameters['destination'] = \Drupal::destination()->getAsArray()['destination'] ?? '';
    // Get the current swarm from the route.
    $swarm = $route_match->getParameter('swarm_id');
    $route_parameters['swarm'] = $swarm;
    return $route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(?Request $request = NULL) {
    $swarm_id = $request->attributes->get('swarm_id');
    $swarm = \Drupal::entityTypeManager()->getStorage('ai_agent')->load($swarm_id);
    return $this->t('Add Project to @swarm', ['@swarm' => $swarm->label()]);
  }

}
