<?php

namespace Drupal\minikanban_agent\Plugin\Block;

use Drupal\ai_agents\Plugin\AiFunctionCall\AiAgentWrapper;
use Drupal\ai_agents\PluginBase\AiAgentEntityWrapper;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a view of a projects agents.
 */
#[Block(
  id: "minikanban_agent_list_agents",
  admin_label: new TranslatableMarkup("Minikanban Agent: List Agents"),
  category: new TranslatableMarkup("Minikanban")
)]
class AgentsView extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the route parameters.
    $route_parameters = \Drupal::routeMatch()->getParameters();
    $project = $route_parameters->get('project');
    if (!$project) {
      return [];
    }
    $content = '';
    if ($project->field_orchestration_agent->entity) {
      $agent_plugin = $project->field_orchestration_agent->entity;
      $content .= '<details class="kanban-agent-list">';
      $content .= $this->t('<summary>Agents available for project</summary>');
      // Create edit link.
      $edit_link = $agent_plugin->toLink($this->t('Edit the Swarm'), 'edit-form', [
        'destination' => \Drupal::destination()->getAsArray()['destination'] ?? '',
      ])->toString();
      $content .= '<p>' . $edit_link . '</p>';

      foreach ($agent_plugin->get('tools') as $tool => $enabled) {
        $function = \Drupal::service('ai.function_calls')->createInstance($tool);
        $content .= '<ul>';
        if ($function instanceof AiAgentWrapper) {
          /** @var \Drupal\ai_agents\PluginBase\AiAgentEntityWrapper */
          $agent_wrapper = $function->getAgent();
          // Get the agent.
          if ($agent_wrapper instanceof AiAgentEntityWrapper) {
            $agent = $agent_wrapper->getAiAgentEntity();
            // Get the edit link.
            $edit_link = $agent->toLink($this->t('Edit Agent'), 'edit-form', [
              'destination' => \Drupal::destination()->getAsArray()['destination'] ?? '',
            ])->toString();

            $content .= '<li>' . $agent->get('label') . ' (' . $edit_link . ')';
            $content .= '<details class="tools">';
            $content .= $this->t('<summary>Tools/Sub-Agents Available</summary>');
            $content .= '<ul>';
            foreach ($agent->get('tools') as $tool_name => $tool_enabled) {
              $tool_instance = \Drupal::service('ai.function_calls')->getDefinition($tool_name);
              $content .= '<li>' . $tool_instance['name'] . '</li>';
            }
            $content .= '</ul>';
            $content .= '</details>';
            $content .= '</li>';
          }
          else {
            $definition = \Drupal::service('plugin.manager.ai_agents')->getDefinition(str_replace('ai_agent:', '', $tool));
            $content .= '<li>' . $definition['label'] . '</li>';
          }
        }
        $content .= '</ul>';
      }
      $content .= '</details>';
    }
    return [
      '#markup' => '<div class="kanban-agent-list-wrapper">' . $content . '</div>',
      '#attached' => [
        'library' => [
          'minikanban_agent/kanban-list-agent',
        ],
      ],
      '#cache' => [
        'contexts' => ['url'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
