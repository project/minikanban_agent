<?php

namespace Drupal\minikanban_agent\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\ai\AiProviderPluginManager;
use Drupal\ai_agents\PluginInterfaces\AiAgentInterface;
use Drupal\ai_agents\PluginManager\AiAgentManager;
use Drupal\minikanban\Entity\Epic;
use Drupal\minikanban\Entity\Task;
use Drupal\minikanban\EpicInterface;
use Drupal\minikanban\TaskHelper;
use Drupal\minikanban\TaskInterface;
use Drupal\minikanban_agent\AgentRunner;
use Drupal\minikanban_agent\Exceptions\AgentRunnerException;
use Drupal\minikanban_agent\TaskGenerator;
use Drupal\minikanban_agent\TaskWrapper;
use Drupal\minikanban_agent\WorkHelper;
use Drupal\user\Entity\User;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting services into this class.
 *
 * @package Drupal\minikanban\Commands
 */
class RunWorkerAgent extends DrushCommands {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $kanbanLogger;

  /**
   * RunWorkerAgent constructor.
   *
   * @param \Drupal\minikanban_agent\AgentRunner $agentRunner
   *   The agent runner.
   * @param \Drupal\ai_agents\PluginManager\AiAgentManager $agentManager
   *   The agent helper.
   * @param \Drupal\minikanban\TaskHelper $taskHelper
   *   The task helper.
   * @param \Drupal\minikanban_agent\WorkHelper $workHelper
   *   The work helper.
   * @param \Drupal\minikanban_agent\TaskGenerator $taskGenerator
   *   The task generator.
   * @param \Drupal\ai\AiProviderPluginManager $aiProviderPluginManager
   *   The AI provider plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(
    protected readonly AgentRunner $agentRunner,
    protected readonly AiAgentManager $agentManager,
    protected readonly TaskHelper $taskHelper,
    protected readonly WorkHelper $workHelper,
    protected readonly TaskGenerator $taskGenerator,
    protected readonly AiProviderPluginManager $aiProviderPluginManager,
    protected readonly EntityTypeManager $entityTypeManager,
    protected readonly ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    $this->kanbanLogger = $loggerChannelFactory->get('minikanban_agent');
  }

  /**
   * Run the Minikanban Agent worker.
   *
   * @command minikanban-agent:run
   * @aliases mia minikanban:agent
   * @usage minikanban-agent:run
   *   Run one Minikanban Agent worker task.
   */
  public function runMinikanbanAgents() {
    $tasksRun = $this->pmTaskGeneration();
    $workersRun = $this->askWorkerAgent();
    if (!$tasksRun || !$workersRun) {
      $this->output()->writeln('No tasks or workers found, cooling down 5 seconds.');
      sleep(1);
    }
  }

  /**
   * Ask the project manager to generate tasks.
   *
   * @return bool
   *   If a task was run.
   */
  public function pmTaskGeneration() {
    $this->output()->writeln('The project manager is looking for new epics.');
    $epic = $this->workHelper->getNextEpicInQueue();
    if (!$epic) {
      $this->output()->writeln('No new epics found.');
      return FALSE;
    }
    /** @var \Drupal\ai\AiProviderInterface $llm */
    $llm = $this->aiProviderPluginManager->createInstance('openai');
    $this->output()->writeln('Starting to generate tasks for epic "' . $epic->label() . '" (' . $epic->id() . ').');
    $this->setEpicStatus($epic, 'in_progress');
    $solution = $this->taskGenerator->generateTasks($epic, $llm);
    $this->setEpicStatus($epic, $solution->getStatusChange());
    $message = $this->createTagMention($epic) . ' ';
    $message .= $solution->getStringResponse();
    $this->createEpicComment($epic, $message);
    // If something failed, the admin is the main user again.
    if ($solution->getStatusChange() !== 'in_progress') {
      $this->reassignEpicToOwner($epic);
    }
    return TRUE;
  }

  /**
   * Runs one job for one work agent if available.
   *
   * @return bool
   *   If a worker was run.
   */
  public function askWorkerAgent() {
    $this->output()->writeln('Our workers are looking for tasks.');
    $work = $this->workHelper->getNextTaskInQueue();
    if (!$work) {
      return FALSE;
    }
    try {
      $task_wrapper = new TaskWrapper($work);
      $task_wrapper->convertKanbanTaskToAgentTask();
      // Start progress.
      $this->setTaskStatus($task_wrapper, 'in_progress');
      // Load the agent instance.
      $agent = $this->loadAgentFromTask($task_wrapper);
      /** @var \Drupal\ai\AiProviderInterface $llm */
      $llm = $this->aiProviderPluginManager->createInstance('openai');
      // Set the agent and LLM.
      $this->agentRunner->setAgent($agent);
      $this->agentRunner->setLlm($llm);

      // Run the agent.
      $this->output()->writeln(t('Running agent :agent (:agent_id: :sub_agent_id) on task :task (:id).', [
        ':agent' => $agent->agentsCapabilities()[$agent->getId()]['name'] ?? 'unknown',
        ':agent_id' => $agent->getId(),
        ':sub_agent_id' => $agent->getId(),
        ':task' => $work->label(),
        ':id' => $work->id(),
      ]));
      $this->reactStatus($this->agentRunner->runAgent($task_wrapper));
      return TRUE;
    }
    catch (\Exception $e) {
      $message = t("Error while trying to solve the issue with the following error: <strong>@error</strong>.<br \><br \> I won't retry again.", [
        '@error' => $e->getMessage(),
      ]);
      $this->kanbanLogger->error($message);
      return TRUE;
    }

    $this->output()->writeln('Done.');
    return TRUE;
  }

  /**
   * Wrapper for the agent runner.
   *
   * @param int $status
   *   The status.
   */
  public function reactStatus($status) {
    switch ($status) {
      case AiAgentInterface::JOB_NOT_SOLVABLE:
        $this->output()->writeln('Job not solvable.');
        break;

      case AiAgentInterface::JOB_SOLVABLE:
        $this->output()->writeln('Job solvable.');
        // Else we try to solve the issue in as many retries as possible.
        try {
          $this->output()->writeln("Trying to solve the issue.");
          $solution = $this->agentRunner->getAgent()->solve();
          $message = $this->createTagMention($this->agentRunner->getTask()->getKanbanTask()) . ' ';
          $message .= $solution;
          $this->createTaskComment($this->agentRunner->getTask()->getKanbanTask(), $message);
          // If a status is set, we set it.
          $this->setTaskStatus($this->agentRunner->getTask(), 'done');
          // If it should be reassigned, we do that.
          //$this->reassignTaskToOwner($this->agentRunner->getTask()->getKanbanTask());
          return;
        }
        catch (\Exception $e) {
          // If it fails we write a comment that it failed.
          $message = t("Error while trying to solve the issue with the following error: <strong>@error</strong>.<br \><br \> I won't retry again.", [
            '@error' => $e->getMessage(),
          ]);
          $this->kanbanLogger->error($message);
        }
        // If all retries went wrong we set the status to failed.
        $this->createTaskComment($this->agentRunner->getTask()->getKanbanTask(), t('I failed to solve the issue, please look in the logs for more information.'));
        break;

      case AiAgentInterface::JOB_NEEDS_ANSWERS:
        $this->output()->writeln('Job needs answers.');
        if (!empty($this->agentRunner->askQuestion())) {
          // Write a comment.
          $message = $this->createTagMention($this->agentRunner->getTask()->getKanbanTask());
          $message .= "To continue, could you answer the following questions and reassign back to me:<br \><ul><li>";
          $message .= implode("</li><li>", $this->agentRunner->askQuestion());
          $message .= "</li></ul>";
          $this->createTaskComment($this->agentRunner->getTask()->getKanbanTask(), $message);
          // Reassign back.
          $this->reassignTaskToOwner($this->agentRunner->getTask()->getKanbanTask());
          // Set status to suggested.
          $this->setTaskStatus($this->agentRunner->getTask(), $this->agentRunner->getSuggestedTaskStatus());
        }
        break;

      case AiAgentInterface::JOB_SHOULD_ANSWER_QUESTION:
        $this->output()->writeln('The task will answer some questions.');
        $message = $this->createTagMention($this->agentRunner->getTask()->getKanbanTask());
        $message .= $this->agentRunner->answerQuestion();
        $this->createTaskComment($this->agentRunner->getTask()->getKanbanTask(), $message);
        $this->setTaskStatus($this->agentRunner->getTask(), 'done');
        break;

      default:
        $this->output()->writeln('Unknown status.');
    }
  }

  /**
   * Create tag mention.
   *
   * @param \Drupal\minikanban\Task\TaskInterface|\Drupal\minikanban\EpicInterface $work
   *   The work.
   * @param \Drupal\user\Entity\User $user
   *   The user to tag.
   *
   * @return string
   *   The tag mention.
   */
  protected function createTagMention(TaskInterface|EpicInterface $work, ?User $user = NULL) {
    $author = $user ?? $work->getOwner();
    $parts = [
      'class' => 'mention',
      'data-mention' => '@' . $author->id(),
      'data-mention-uuid' => $work->uuid(),
      'data-entity-type' => 'user',
      'data-entity-uuid' => $author->uuid(),
      'data-plugin' => 'user',
      'href' => '/user/' . $author->id(),
    ];
    $tag = '<a ';
    foreach ($parts as $key => $value) {
      $tag .= $key . '="' . $value . '" ';
    }
    $tag .= '>@' . $author->getDisplayName() . '</a> ';
    return $tag;
  }

  /**
   * Create a comment on the task or epic.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   * @param string $message
   *   The message.
   *
   * @return \Drupal\minikanban\Entity\Comment
   *   The comment.
   */
  protected function createTaskComment(Task $task, $message) {
    $storage = $this->entityTypeManager->getStorage('kanban_comment');
    $comment = $storage->create([
      'task' => $task->id(),
      'project' => $task->getProject()->id(),
      'comment' => [
        'value' => $message,
        'format' => 'kanban_html',
      ],
      'uid' => $this->configFactory->get('ai_interpolator_agent.settings')->get('main_user'),
    ]);
    $comment->save();
    return $comment;
  }

  /**
   * Reassign the task back to the creator.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   *
   * @return \Drupal\minikanban\Entity\Task
   *   The task.
   */
  protected function reassignTaskToOwner(Task $task) {
    $task->setAssignee($task->getOwner());
    $task->save();
    return $task;
  }

  /**
   * Reassign the status of the task.
   *
   * @param \Drupal\minikanban_agent\TaskWrapper $task
   *   The task.
   * @param string $status
   *   The status.
   *
   * @return \Drupal\minikanban_agent\TaskWrapper
   *   The task.
   */
  protected function setTaskStatus(TaskWrapper $task, $status): TaskWrapper {
    $task->getKanbanTask()->setStatus($status);
    $task->getKanbanTask()->save();
    return $task;
  }

  /**
   * Create a comment on the task or epic.
   *
   * @param \Drupal\minikanban\Entity\Epic $epic
   *   The epic.
   * @param string $message
   *   The message.
   *
   * @return \Drupal\minikanban\Entity\Comment
   *   The comment.
   */
  protected function createEpicComment(Epic $epic, $message) {
    $storage = $this->entityTypeManager->getStorage('kanban_comment');
    $comment = $storage->create([
      'epic' => $epic->id(),
      'project' => $epic->getProject()->id(),
      'comment' => [
        'value' => $message,
        'format' => 'kanban_html',
      ],
      'uid' => $this->configFactory->get('ai_interpolator_agent.settings')->get('main_user'),
    ]);
    $comment->save();
    return $comment;
  }

  /**
   * Reassign the epic back to the creator.
   *
   * @param \Drupal\minikanban\Entity\Epic $epic
   *   The epic.
   *
   * @return \Drupal\minikanban\Entity\Epic
   *   The epic.
   */
  protected function reassignEpicToOwner(Epic $epic) {
    $epic->setAssignee($epic->getOwner());
    $epic->save();
    return $epic;
  }

  /**
   * Reassign the status of the epic.
   *
   * @param \Drupal\minikanban\Entity\Epic $epic
   *   The epic.
   * @param string $status
   *   The status.
   *
   * @return \Drupal\minikanban\Entity\Epic
   *   The epic.
   */
  protected function setEpicStatus(Epic $epic, $status) {
    $epic->setStatus($status);
    $epic->save();
    return $epic;
  }

  /**
   * Figure out agent and sub agent from the task.
   *
   * @param \Drupal\minikanban_agent\TaskWrapper $task
   *   The task.
   *
   * @return \Drupal\ai_agents\PluginInterfaces\AiAgentInterface
   *   The agent.
   */
  public function loadAgentFromTask(TaskWrapper $task) {
    if ($task->getKanbanTask()->ai_agent->value) {
      $agent = explode('__', $task->getKanbanTask()->ai_agent->value);
      if (count($agent) === 2) {
        $agentInstance = $this->agentManager->createInstance($agent[0]);
        $newTask = $task->getAiTask();
        // Make sure to convert the task.
        $agentInstance->setTask($newTask);
        return $agentInstance;
      }
    }
    throw new AgentRunnerException('No agent and sub-agent found.');
  }

}
