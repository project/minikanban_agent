<?php

namespace Drupal\minikanban_agent;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\ai\AiProviderPluginManager;
use Drupal\ai\OperationType\Chat\ChatInput;
use Drupal\ai\OperationType\Chat\ChatMessage;
use Drupal\ai\Service\PromptCodeBlockExtractor\PromptCodeBlockExtractorInterface;
use Drupal\minikanban\Entity\Task;
use Drupal\minikanban_agent\Exceptions\AgentRunnerException;
use Drupal\minikanban_agent\PluginManager\MinikanbanAgentManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Helper for worker agents.
 */
class AgentHelper {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param \Drupal\minikanban_agent\PluginManager\MinikanbanAgentManager $agentManager
   *   The agent manager.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   * @param \Drupal\ai\AiProviderPluginManager $aiProvider
   *   The AI provider plugin manager.
   * @param \Drupal\ai\Service\PromptCodeBlockExtractor\PromptCodeBlockExtractorInterface $promptCodeBlockExtractor
   *   The prompt code block extractor.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory interface.
   */
  public function __construct(
    private readonly MinikanbanAgentManager $agentManager,
    private readonly ExtensionPathResolver $extensionPathResolver,
    private readonly AiProviderPluginManager $aiProvider,
    private readonly PromptCodeBlockExtractorInterface $promptCodeBlockExtractor,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->config = $configFactory->get('minikanban_agent.settings');
  }

  /**
   * Gets the main user.
   *
   * @return string
   *   The main user.
   */
  public function getMainUser() {
    return $this->config->get('main_user');
  }

  /**
   * Builds action prompts.
   *
   * @param string $module
   *   The module name to fetch prompt yamls from.
   * @param string $type
   *   The type of prompt to fetch.
   * @param array $userPrompts
   *   The user prompts to add to the action prompt.
   *
   * @return array|null
   *   The action prompt and the model to use.
   */
  public function actionPrompts($module, $type, array $userPrompts) {
    // Developers makes mistakes.
    $file = $this->extensionPathResolver->getPath('module', $module) . '/prompts/' . basename($type, '.yml') . '.yml';
    if (!file_exists($file)) {
      throw new AgentRunnerException("The action prompt file '$file' does not exist.");
    }
    $data = Yaml::parse(file_get_contents($file));
    // Set introduction.
    $prompt = $data['introduction'] . "\n\n";
    // Set formats to use.
    $invariable = count($data['formats']) == 1 ? 'this format' : 'these formats';
    $structure = "";
    foreach ($data['formats'] as $format) {
      $structure .= json_encode($format) . "\n";
    }
    $prompt .= "Do not include any explanations, only provide a RFC8259 compliant JSON response following $invariable without deviation:\n[$structure]\n";
    if (!empty($data['possible_actions'])) {
      $prompt .= "This is the list of actions:\n";
      foreach ($data['possible_actions'] as $action => $description) {
        $prompt .= "$action - $description\n";
      }
    }
    $prompt .= "\n";
    if (!empty($data['one_shot_learning_examples'])) {
      $prompt .= "Example response given\n";
      $prompt .= json_encode($data['one_shot_learning_examples']) . "\n";
    }
    foreach ($userPrompts as $header => $userPrompt) {
      $prompt .= "\n\n-----------------------------------\n$header:\n$userPrompt\n-----------------------------------";
    }

    return [
      'prompt' => $prompt,
      'preferred_llm' => $data['preferred_llm'] ?? 'openai',
      'preferred_model' => $data['preferred_model'] ?? 'gpt-3.5-turbo',
    ];
  }

  /**
   * Get the finished contexts of an epic if needed, when tasks are in_review.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task to move the context from.
   */
  public function checkIfEpicIsDone(Task $task) {
    // Get the epic.
    $epic = $task->getEpic();
    if ($epic == NULL || $epic->status->value != 'in_progress') {
      return;
    }
    // Check if all tasks are done.
    $epic_tasks = $epic->getTasks();
    foreach ($epic_tasks as $epicTask) {
      if ($epicTask->status->value != 'done') {
        return;
      }
    }

    // If all tasks are done, we move the context and set the epic to in_review.
    $comment = t("All tasks are done, the following results hopefully fits what you were looking for. If not please check tasks where it went wrong and rewrite a new epic.<br /><br />");
    foreach ($epic_tasks as $epic_task) {
      $commments = $epic_task->getComments();
      foreach ($commments as $comment_entity) {
        $comment .= $comment_entity->comment->value;
        $comment .= "<br />";
      }
    }

    // Rewrite the comment using an LLM.
    $defaults = $this->aiProvider->getDefaultProviderForOperationType('chat');
    $llm = $this->aiProvider->createInstance($defaults['provider_id']);
    $input = new ChatInput([
      new ChatMessage("user", "Look at the following results from different agents and try to summarize the results into a single comment. Please respond with HTML in an html code block, using a, em, strong, ul, ol, li, img tags where needed. The results are:<br /><br />$comment"),
    ]);
    $response = $llm->chat($input, $defaults['model_id']);
    $html = $this->promptCodeBlockExtractor->extract($response->getNormalized(), 'html');

    // Add comment.
    $epic->addComment($html, $this->config->get('main_user'));
    $epic->status = 'in_review';
    $epic->save();
  }

  /**
   * Move a finished context into the next dependent task.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task to move the context from.
   */
  public function moveContextBetweenTasks(Task $task) {
    // Load the main user id.
    $mainUserId = $this->config->get('main_user');
    // Check if blockers are attached to this task.
    $blockers = $task->getBlockingTasks();
    if (!empty($blockers)) {
      // If it has blocking tasks, comment the result on them.
      foreach ($blockers as $blocker) {
        // Load the agent.
        $part = explode('__', $task->ai_agent->value);
        if (!count($part) == 2) {
          continue;
        }
        $comments = $task->getComments();
        // Add a comment to the blocked ticket.
        $comment = t("A blocking ticket was completed and has context that this ticket needs to finish. See the results below:<br /><br />");
        foreach ($comments as $comment_entity) {
          $comment .= $comment_entity->comment->value . "<br />";
        }
        $comment = $blocker->addComment($comment, $mainUserId);
      }
    }
  }

}
