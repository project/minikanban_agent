<?php

namespace Drupal\minikanban_agent;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\minikanban\Entity\Task;
use Drupal\minikanban_agent\Exceptions\AgentRunnerException;
use Drupal\minikanban_agent\PluginInterfaces\MinikanbanLlmInterface;

/**
 * Helper for worker agents.
 */
class AgentPlugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The sub agent.
   *
   * @var string
   */
  protected $subAgent;

  /**
   * The task.
   *
   * @var \Drupal\minikanban\Entity\Task
   */
  protected Task $task;

  /**
   * The LLM.
   *
   * @var \Drupal\minikanban_agent\PluginInterfaces\MinikanbanLlmInterface
   */
  protected MinikanbanLlmInterface $llm;

  /**
   * The agent helper.
   *
   * @var \Drupal\minikanban_agent\AgentHelper
   */
  protected $agentHelper;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->agentHelper = $this->getAgentHelper();
    $this->fileSystem = $this->getFileSystem();
    $this->config = $this->getConfigFactory();
  }

  /**
   * {@inheritDoc}
   */
  public function setTask(Task $task) {
    $this->task = $task;
  }

  /**
   * {@inheritDoc}
   */
  public function getTask() {
    return $this->task;
  }

  /**
   * {@inheritDoc}
   */
  public function setSubAgent($subAgent) {
    $this->subAgent = $subAgent;
  }

  /**
   * {@inheritDoc}
   */
  public function getSubAgent() {
    return $this->subAgent;
  }

  /**
   * {@inheritDoc}
   */
  public function getSubAgentTitle() {
    return $this->agentsCapabilities()[$this->subAgent] ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function agentsCapabilities() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getResults() {
    return $this->getState()['result'] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function setResults(array $results) {
    $state = $this->getState();
    $state['result'] = $results;
    return $this->setState($state);
  }

  /**
   * {@inheritDoc}
   */
  public function setState(array $state) {
    $task = $this->getTask();
    $upsert = \Drupal::database()->upsert('kanban_agent_state')
      ->key('task_id')
      ->fields([
        'task_id' => $task->id(),
        'state' => json_encode($state),
      ]);
    return $upsert->execute() ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getState() {
    $task = $this->getTask();
    $query = \Drupal::database()->select('kanban_agent_state', 'kas');
    $query->fields('kas', ['state']);
    $query->condition('task_id', $task->id());
    $result = $query->execute()->fetchField();
    return $result ? json_decode($result, TRUE) : [];
  }


  /**
   * Sets the llm.
   *
   * @param MinikanbanLlmInterface $llm
   *   The llm.
   */
  public function setLlm(MinikanbanLlmInterface $llm) {
    $this->llm = $llm;
  }

  /**
   * Get cleaned inputs.
   *
   * @return array
   *   The cleaned inputs.
   */
  public function getCleanedInputs(Task $task) {
    $context = $this->getFullContextOfTask($task, FALSE);
    $capabilities = $this->agentsCapabilities();
    $cleanedInputs = [];
    foreach ($capabilities[key($capabilities)]['inputs'] as $key => $input) {
      $cleanedInputs[$key] = $this->getCleanedInput($input, $context);
    }
    return $cleanedInputs;
  }

  /**
   * Get cleaned input.
   *
   * @param array $input
   *   The input.
   * @param string $context
   *   The context.
   *
   * @return string
   *   The cleaned input.
   */
  public function getCleanedInput(array $input, $context) {
    $inputData = "";
    foreach ($input as $key => $value) {
      $inputData .= "$key: " . json_encode($value) . "\n";
    }
    $finalPrompt = $this->agentHelper->actionPrompts('minikanban_agent', 'cleanInput', [
      'Prompt (A PM ticket and comments)' => $context,
      'Input description' => $inputData,
    ]);
    $actions = $this->cleanupAndDecodeJsonResponse($this->llm->prompt($finalPrompt['prompt'], $finalPrompt));
    $outputData = [];
    if (isset($actions[0]['value'])) {
      foreach ($actions as $action) {
        $outputData[] = $action['value'];
      }
    }
    return $outputData;
  }

  /**
   * Find latest question.
   *
   * @param Task $task
   *   The task.
   */
  public function findLatestQuestion(Task $task) {
    $this->getFullContextOfTask($task);

    // Get the main user.
    $mainUser = $task->ai_worker->entity->label() ?? NULL;
    if (!$mainUser) {
      throw  new AgentRunnerException('No connected AI agent found.');
    }
    $prompt = "Based on the following context, can you find the unanswered question for the user $mainUser?";
    return $this->llm->prompt($prompt);
  }

  /**
   * Cleanup and decode json response.
   *
   * @param string $response
   *   The response.
   *
   * @return array
   *   The response.
   */
  public function cleanupAndDecodeJsonResponse($response) {
    // Remove white lines at start and finish.
    $response = trim($response);
    // Remove json notation.
    $response = str_replace(['```json', '```'], '', $response);
    // Decode the json.
    return json_decode($response, TRUE);
  }

  /**
   * Get full context of the task.
   *
   * @param Task $task
   *   The task.
   * @param bool $stripTags
   *   Strip tags.
   *
   * @return string
   *   The context.
   */
  public function getFullContextOfTask(Task $task, $stripTags = TRUE) {
    // Get the description and the comments.
    $context = "Task Title: " . $task->getTitle() . "\n";
    $context .= "Task Author: " . $task->getAuthorsUsername() . "\n";
    $context .= "Task Description:\n" . $task->getDescription();
    $context .= "\n--------------------------\n";
    $comments = $task->getComments();
    if (count($comments)) {
      $context .= "These are the following comments:\n";
      $context .= "--------------------------\n";

      $i = 1;
      foreach ($comments as $comment) {
        $context .= "Comment order $i: \n";
        $context .= "Comment Author: " . $comment->getAuthorsUsername() . "\n";
        // TODO: Add nicer cleaner.
        $context .= "Comment:\n" . str_replace(["<br>", "<br />", "<br/>"], "\n", $comment->getComment()) . "\n\n";
        $i++;
      }
      $context .= "--------------------------\n";
    }
    return $stripTags ? strip_tags($context) : $context;
  }

  /**
   * Helper to output an image to a comment.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   *
   * @return string
   *   The image html.
   */
  public function outputFileToComment(File $file) {
    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    return '<a href="' . $url . '" width="500" data-entity-uuid="' . $file->uuid() . '">' . $file->getFilename() . '</a>';
  }

  /**
   * Helper to output an image to a comment.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file.
   *
   * @return string
   *   The image html.
   */
  public function outputImageToComment(File $file) {
    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    return '<img src="' . $url . '" width="500" data-entity-uuid="' . $file->uuid() . '">';
  }

  /**
   * Helper function to output string to comment.
   *
   * @param string $string
   *   The string.
   *
   * @return string
   *   The HTML string.
   */
  public function outputStringToComment($string) {
    return str_replace("\n", "<br />", $string);
  }

  /**
   * Helper function to get description.
   *
   * @return string
   *   The description.
   */
  public function getDescription() {
    $capabilities = $this->agentsCapabilities();
    return $capabilities[key($capabilities)]['description'] ?? '';
  }

  /**
   * Get inputs as string.
   *
   * @return string
   *   The string.
   */
  public function getInputsAsString() {
    $capabilities = $this->agentsCapabilities();
    $outputString = "";
    foreach ($capabilities as $data) {
      foreach ($data['inputs'] as $input) {
        $outputString .= "Field name: $input[name] ($input[type]):\n";
        $outputString .= "Required:";
        $outputString .= isset($input['required']) && $input['required'] ? "Yes\n" : "No\n";
        $outputString .= "Description: $input[description]\n\n";
      }
    }
    return $outputString;
  }

  /**
   * Get outputs as string.
   *
   * @return string
   *   The string.
   */
  public function getOutputsAsString() {
    $capabilities = $this->agentsCapabilities();
    $outputString = "";
    foreach ($capabilities as $data) {
      foreach ($data['outputs'] as $title => $output) {
        $outputString .= "Field name: $title ($output[type]):\n";
        $outputString .= "Description: $output[description]\n";
      }
    }
    return $outputString;
  }

  /**
   * Get help.
   *
   * @return \Drupal\minikanban_agent\AgentHelper
   *   The helper.
   */
  private function getAgentHelper() {
    return \Drupal::service('minikanban_agent.agent_helper');
  }

  /**
   * Get file system.
   *
   * @return \Drupal\Core\File\FileSystem
   *   The file system.
   */
  private function getFileSystem() {
    return \Drupal::service('file_system');
  }

  /**
   * Get the config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The config factory.
   */
  private function getConfigFactory() {
    return \Drupal::configFactory();
  }

}
