<?php

namespace Drupal\minikanban_agent;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class WorkHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * WorkHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->config = $configFactory->get('minikanban_agent.settings');
  }

  /**
   * Get the next in the work queue.
   *
   * @return \Drupal\minikanban\Entity\Task|null
   *   The next task in the queue, or NULL if there are no tasks.
   */
  public function getNextTaskInQueue() {
    // Get the storage.
    $storage = $this->entityTypeManager->getStorage('kanban_task');
    // Get the query.
    $query = $storage->getQuery();
    // Get the result.
    $result = $query->condition('ai_agent', '', '<>')
      ->condition('status', 'todo')
      ->condition('assigned', $this->config->get('main_user'))
      ->accessCheck(TRUE)
      ->sort('created', 'ASC')
      ->range(0, 1)
      ->execute();
    // Return the result.
    return $result ? $storage->load(key($result)) : NULL;
  }

  /**
   * Get the next epic in queue.
   *
   * @return \Drupal\minikanban\Entity\Epic|null
   *   The next epic in the queue, or NULL if there are no epics.
   */
  public function getNextEpicInQueue() {
    // Get the storage.
    $storage = $this->entityTypeManager->getStorage('kanban_epic');
    // Get the query.
    $query = $storage->getQuery();
    // Get the result.
    $result = $query->condition('status', 'todo')
      ->condition('assigned', $this->config->get('main_user'))
      ->accessCheck(TRUE)
      ->sort('created', 'ASC')
      ->range(0, 1)
      ->execute();
    // Return the result.
    return $result ? $storage->load(key($result)) : NULL;
  }

}
