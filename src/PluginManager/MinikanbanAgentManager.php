<?php

namespace Drupal\minikanban_agent\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a Minikanban Agent plugin manager.
 *
 * @see \Drupal\minikanban_agent\Annotation\MinikanbanAgent
 * @see \Drupal\minikanban_agent\PluginInterfaces\MinikanbanAgentInterface
 * @see plugin_api
 */
class MinikanbanAgentManager extends DefaultPluginManager {

  /**
   * Constructs a MinikanbanAgentInterface object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/MinikanbanAgent',
      $namespaces,
      $module_handler,
      'Drupal\minikanban_agent\PluginInterfaces\MinikanbanAgentInterface',
      'Drupal\minikanban_agent\Annotation\MinikanbanAgent'
    );
    $this->alterInfo('minikanban_agent_agent');
    $this->setCacheBackend($cache_backend, 'minikanban_agent_agent_plugins');
  }

}
