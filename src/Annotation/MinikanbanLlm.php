<?php

namespace Drupal\minikanban_agent\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Declare an Minikanban LLM.
 *
 * Comes with the simplest solution to inherit for functions.
 *
 * @ingroup minikanban_agent_llm
 *
 * @Annotation
 */
class MinikanbanLlm extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable title of the plugin.
   *
   * @var Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public $title;
}
