<?php

namespace Drupal\minikanban_agent\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\minikanban\Entity\Task;
use Drupal\minikanban_agent\Exceptions\AgentRunnerException;
use Drupal\minikanban_agent\PluginInterfaces\MinikanbanLlmInterface;

/**
 * Declare an Minikanban Agent.
 *
 * Comes with the simplest solution to inherit for functions.
 *
 * @ingroup minikanban_agent_agent
 *
 * @Annotation
 */
class MinikanbanAgent extends Plugin {

  // All should be translatable.
  use StringTranslationTrait;

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable title of the plugin.
   *
   * @var Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The LLM.
   *
   * @var \Drupal\minikanban_agent\PluginInterfaces\MinikanbanLlmInterface
   */
  protected MinikanbanLlmInterface $llm;

  /**
   * The sub agent.
   *
   * @var string
   */
  protected $subAgent;

  /**
   * The task.
   *
   * @var \Drupal\minikanban\Entity\Task
   */
  protected Task $task;

}
