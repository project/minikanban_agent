<?php

namespace Drupal\minikanban_agent\AgentSolutions;

/**
 * This should be used when the agent needs time to answer. For instance
 * when the agent needs to call an external API and poll it.
 */
class AsynchronousSolution implements AgentSolutionInterface {

  /**
   * The response from the agent.
   *
   * @var string
   */
  protected string $response;

  /**
   * The files response from the agent.
   *
   * @var \Drupal\file\Entity\File[]|null
   */
  protected array|null $files;

  /**
   * The taskStatuses key to change to.
   *
   * @var string
   */
  protected string $statusChange;

  /**
   * DirectSolution constructor.
   *
   * @param string $response
   *   The response from the agent.
   * @param \Drupal\file\Entity\File[]|null $files
   *   The files response from the agent.
   * @param string $statusChange
   *   The taskStatuses key to change to.
   */
  public function __construct(string $response, array|null $files, string $statusChange) {
    $this->response = $response;
    $this->files = $files;
    $this->statusChange = $statusChange;
  }

  /**
   * Get the string response from the agent.
   *
   * @return string
   *   The response from the agent.
   */
  public function setStringResponse() {
    return $this->response;
  }

  /**
   * Get the file response from the agent.
   *
   * @return \Drupal\file\Entity\File[]|null
   *   The files response from the agent.
   */
  public function setFileResponse(): array|null {
    return $this->files;
  }

  /**
   * Get initial status change.
   *
   * @return string
   *   The taskStatuses key to change to.
   */
  public function setStatusChange(): string {
    return $this->statusChange;
  }
}
