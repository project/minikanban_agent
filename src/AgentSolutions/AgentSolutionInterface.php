<?php

namespace Drupal\minikanban_agent\AgentSolutions;

/**
 * Interface for Minikanban Agents solutions.
 */
interface AgentSolutionInterface {

  /**
   * Get the string response from the agent.
   *
   * @return string
   *   The response from the agent.
   */
  public function getStringResponse();

  /**
   * Get the file response from the agent.
   *
   * @return \Drupal\file\Entity\File[]|null
   *   The files response from the agent.
   */
  public function getFileResponse(): array|null;

  /**
   * Get initial status change.
   *
   * @return string|null
   *   The taskStatuses key to change to.
   */
  public function getStatusChange(): string|null;

  /**
   * Should the ticket be reassigned to the creator.
   *
   * @return bool
   *   If the ticket should be reassigned to the creator.
   */
  public function reassignToCreator(): bool;

}
