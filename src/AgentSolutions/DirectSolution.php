<?php

namespace Drupal\minikanban_agent\AgentSolutions;

/**
 * This should be used when the agent can answer within a worker Drush job.
 */
class DirectSolution implements AgentSolutionInterface {

  /**
   * The response from the agent.
   *
   * @var string
   */
  protected string $response;

  /**
   * The files response from the agent.
   *
   * @var \Drupal\file\Entity\File[]|null
   */
  protected array|null $files;

  /**
   * The taskStatuses key to change to.
   *
   * @var string
   */
  protected string $statusChange;

  /**
   * If the ticket should be reassigned to the creator.
   *
   * @var bool
   */
  protected bool $reassign;

  /**
   * DirectSolution constructor.
   *
   * @param string $response
   *   The response from the agent.
   * @param \Drupal\file\Entity\File[]|null $files
   *   The files response from the agent.
   * @param bool $reassign
   *   If the ticket should be reassigned to the creator.
   * @param string $statusChange
   *   The taskStatuses key to change to.
   */
  public function __construct(string $response, string $statusChange = NULL, $reassign = FALSE, array $files = []) {
    $this->response = $response;
    $this->files = $files;
    $this->reassign = $reassign;
    $this->statusChange = $statusChange;
  }

  /**
   * {@inheritDoc}
   */
  public function getStringResponse() {
    return $this->response;
  }

  /**
   * {@inheritDoc}
   */
  public function getFileResponse(): array|null {
    return $this->files;
  }

  /**
   * {@inheritDoc}
   */
  public function getStatusChange(): string {
    return $this->statusChange;
  }

  /**
   * {@inheritDoc}
   */
  public function reassignToCreator(): bool {
    return $this->reassign;
  }

}
