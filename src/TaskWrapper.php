<?php

namespace Drupal\minikanban_agent;

use Drupal\ai_agents\Task\Task as TaskTask;
use Drupal\ai_agents\Task\TaskInterface;
use Drupal\minikanban\Entity\Task;

/**
 * This is a wrapper around the two different task interfaces.
 */
class TaskWrapper {

  /**
   * The Kanban task.
   *
   * @var \Drupal\minikanban\Entity\Task
   */
  protected ?Task $kanbanTask = NULL;

  /**
   * The AI Agents task.
   *
   * @var \Drupal\ai_agents\Task\TaskInterface
   */
  protected ?TaskInterface $aiTask = NULL;

  /**
   * TaskWrapper constructor.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   * @param \Drupal\ai_agents\Task\TaskInterface $aiTask
   *   The AI Agents task.
   */
  public function __construct(?Task $task = NULL, ?TaskInterface $aiTask = NULL) {
    if ($task) {
      $this->kanbanTask = $task;
    }
    if ($aiTask) {
      $this->aiTask = $aiTask;
    }
  }

  /**
   * Set Kanban task.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   */
  public function setTask(Task $task) {
    $this->kanbanTask = $task;
  }

  /**
   * Get Kanban task.
   *
   * @return \Drupal\minikanban\Entity\Task
   *   The task.
   */
  public function getKanbanTask() {
    return $this->kanbanTask;
  }

  /**
   * Set AI Agents task.
   *
   * @param \Drupal\ai_agents\Task\TaskInterface $task
   *   The task.
   */
  public function setAiTask(TaskInterface $task) {
    $this->aiTask = $task;
  }

  /**
   * Get AI Agents task.
   *
   * @return \Drupal\ai_agents\Task\TaskInterface
   *   The task.
   */
  public function getAiTask() {
    return $this->aiTask;
  }

  /**
   * Convert a Minikanban Task, to an agent Task.
   *
   * @return \Drupal\ai_agents\Task\TaskInterface
   *   The task for the AI Agent.
   */
  public function convertKanbanTaskToAgentTask() {
    $new_task = new TaskTask($this->kanbanTask->description->value);
    $new_task->setTitle($this->kanbanTask->label());
    $comment_entities = $this->kanbanTask->getComments();
    $comments = [];
    foreach ($comment_entities as $comment_entity) {
      $comments[] = [
        'role' => $comment_entity->uid->entity->getDisplayName(),
        'message' => $comment_entity->comment->value,
      ];
    }
    $new_task->setFiles($this->kanbanTask->getAttachedFiles());
    $new_task->setComments($comments);
    $this->aiTask = $new_task;
    return $new_task;
  }

}
