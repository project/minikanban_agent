<?php

namespace Drupal\minikanban_agent;

use Drupal\ai\AiProviderInterface;
use Drupal\ai\Plugin\ProviderProxy;
use Drupal\ai_agents\PluginInterfaces\AiAgentInterface;
use Drupal\minikanban_agent\Exceptions\AgentRunnerException;
use Drupal\minikanban_agent\PluginInterfaces\MinikanbanAgentInterface;
use Drupal\user\Entity\User;

/**
 * The controls the logic of the agents.
 */
class AgentRunner {

  /**
   * The agent.
   *
   * @var \Drupal\ai_agents\PluginInterfaces\AiAgentInterface
   */
  protected AiAgentInterface $agent;

  /**
   * The Llm.
   *
   * @var \Drupal\ai\AiProviderInterface|\Drupal\ai\Plugin\ProviderProxy
   */
  protected AiProviderInterface|ProviderProxy $llm;

  /**
   * The status.
   *
   * @var int
   */
  protected int $status;

  /**
   * The status context.
   *
   * @var array
   */
  protected array $statusContext;

  /**
   * Suggested task status.
   *
   * @var string
   */
  protected string $suggestedTaskStatus;

  /**
   * Get the current task wrapper.
   *
   * @var \Drupal\minikanban_agent\TaskWrapper
   */
  protected TaskWrapper $taskWrapper;

  /**
   * Set the agent.
   *
   * @param \Drupal\ai_agents\PluginInterfaces\AiAgentInterface $agent
   *   The agent.
   */
  public function setAgent(AiAgentInterface $agent) {
    $this->agent = $agent;
  }

  /**
   * Get the agent.
   *
   * @return \Drupal\ai_agents\PluginInterfaces\AiAgentInterface
   *   The agent.
   */
  public function getAgent() {
    return $this->agent;
  }

  /**
   * Get status context.
   *
   * @return array
   *   The status context.
   */
  public function getStatusContext() {
    return $this->statusContext;
  }

  /**
   * Set status context.
   *
   * @param array $statusContext
   *   The status context.
   */
  public function setStatusContext($statusContext) {
    $this->statusContext = $statusContext;
  }

  /**
   * Set suggested task status.
   */
  public function setSuggestedTaskStatus($suggestedTaskStatus) {
    $this->suggestedTaskStatus = $suggestedTaskStatus;
  }

  /**
   * Get suggested task status.
   *
   * @return string
   *   The suggested task status.
   */
  public function getSuggestedTaskStatus() {
    return $this->suggestedTaskStatus;
  }

  /**
   * Set llm.
   *
   * @param \Drupal\ai\AiProviderInterface|\Drupal\ai\Plugin\ProviderProxy $llm
   *   The llm.
   */
  public function setLlm(AiProviderInterface|ProviderProxy $llm) {
    $this->llm = $llm;
    // Also set in the agent.
    if (empty($this->agent)) {
      throw new AgentRunnerException('Agent not set.');
    }
    $this->agent->setAiProvider($llm);
    $this->agent->setModelName('gpt-4o');
  }

  /**
   * Runs one agent end to end.
   *
   * @return bool|string
   *   The result of the agent.
   */
  public function runAgent(TaskWrapper $task) {
    $this->taskWrapper = $task;
    $this->agent->setTask($task->getAiTask());

    // We upgrade the task to the task creator.
    \Drupal::service('account_switcher')->switchTo(User::load($task->getKanbanTask()->getOwnerId()));
    // First we figure out if the agent can solve the issue.
    $this->agent->setCreateDirectly(TRUE);
    $this->status = $this->determineSolvability();

    // If its not solvable, we return early.
    if ($this->status === AiAgentInterface::JOB_NOT_SOLVABLE) {
      // Set status to closed.
      $this->setSuggestedTaskStatus('closed');
      return MinikanbanAgentInterface::JOB_NOT_SOLVABLE;
    }
    // If the agent needs answers, we return early.
    if ($this->status === AiAgentInterface::JOB_NEEDS_ANSWERS) {
      // Set status to todo and give back the questions.
      $this->setStatusContext($this->askQuestion());
      $this->setSuggestedTaskStatus('todo');
      return AiAgentInterface::JOB_NEEDS_ANSWERS;
    }
    // If the agent needs to answer a question, we return early.
    if ($this->status === AiAgentInterface::JOB_SHOULD_ANSWER_QUESTION) {
      return AiAgentInterface::JOB_SHOULD_ANSWER_QUESTION;
    }

    // Else we try to solve the issue.
    return AiAgentInterface::JOB_SOLVABLE;
  }

  /**
   * Control if an agent can solve a specific issue.
   *
   * @return int
   *   One of the JOB_* constants.
   */
  public function determineSolvability() {
    return $this->agent->determineSolvability();
  }

  /**
   * Answer a question from the PM AI or end-users.
   *
   * @return array
   *   The answer to the question.
   */
  public function answerQuestion() {
    return $this->agent->answerQuestion();
  }

  /**
   * Ask a question to the PM AI or end-users to solve the issue.
   *
   * @return array
   *   The question to be asked to the PM AI or end-users.
   */
  public function askQuestion() {
    return $this->agent->askQuestion();
  }

  /**
   * Solve issue.
   *
   * @return bool|string
   *   The result of the agent.
   */
  public function solve() {
    return $this->agent->solve();
  }

  /**
   * Get the tag.
   *
   * @return \Drupal\minikanban_agent\TaskWrapper
   *   The task.
   */
  public function getTask(): TaskWrapper {
    return $this->taskWrapper;
  }

}
