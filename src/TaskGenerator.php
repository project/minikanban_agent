<?php

namespace Drupal\minikanban_agent;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\ai\AiProviderInterface;
use Drupal\ai\AiProviderPluginManager;
use Drupal\ai\OperationType\Chat\ChatInput;
use Drupal\ai\OperationType\Chat\ChatMessage;
use Drupal\ai\Plugin\ProviderProxy;
use Drupal\ai_agents\PluginInterfaces\AiAgentInterface;
use Drupal\ai_agents\PluginManager\AiAgentManager;
use Drupal\ai_agents\Task\Task;
use Drupal\minikanban\Entity\Epic;
use Drupal\minikanban_agent\AgentSolutions\DirectSolution;
use Drupal\minikanban_agent\Exceptions\FaultyAgentException;

/**
 * Task generator.
 */
class TaskGenerator {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The epic.
   *
   * @var \Drupal\minikanban\Entity\Epic
   */
  protected $epic;

  /**
   * The AI Provider Interface.
   *
   * @var \Drupal\ai\AiProviderInterface|\Drupal\ai\Plugin\ProviderProxy
   */
  protected AiProviderInterface|ProviderProxy $llm;

  /**
   * Internal task ids, for blockers.
   *
   * @var array
   */
  protected $blockersTranslation = [];

  /**
   * TaskGenerator constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   * @param \Drupal\ai_agents\PluginManager\AiAgentManager $agentManager
   *   The AI Agent Manager.
   * @param \Drupal\ai\AiProviderPluginManager $aiProvider
   *   The AI provider plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ExtensionPathResolver $extensionPathResolver,
    private readonly AiAgentManager $agentManager,
    private readonly AiProviderPluginManager $aiProvider,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->config = $configFactory->get('minikanban_agent.settings');
  }

  /**
   * Generate tasks from epic.
   *
   * @param \Drupal\minikanban\Entity\Epic $epic
   *   The epic.
   * @param \Drupal\ai\AiProviderInterface|\Drupal\ai\Plugin\ProviderProxy $llm
   *   The LLM.
   *
   * @return \Drupal\minikanban_agent\AgentSolutions\SolutionInterface
   *   The solution.
   */
  public function generateTasks(Epic $epic, AiProviderInterface|ProviderProxy $llm) {
    $this->epic = $epic;
    $this->llm = $llm;
    // Get the project from the epic.
    $project = $this->epic->getProject();
    // Check the orchestration agent.
    $orchestration_agent = $project->field_orchestration_agent->entity;
    // Set some tags.
    $tags = [
      'minikanban_orchestration',
      'minikanban_orchestration_project_' . $project->id(),
      'minikanban_orchestration_epic_' . $epic->id(),
    ];
    $defaults = $this->aiProvider->getDefaultProviderForOperationType('chat_with_complex_json');
    $provider = $this->aiProvider->createInstance($defaults['provider_id']);
    if (!$orchestration_agent) {
      // Let Minikanban decide.
      $prompt = $this->generateEpicToTaskPrompt();
      $result = $provider->chat(new ChatInput([
        new ChatMessage('user', $prompt),
      ]), $defaults['model_id'], $tags)->getNormalized();
      $response = $this->cleanupAndDecodeJsonResponse($result->getText());
      if (empty($response) || !isset($response['status'])) {
        throw new FaultyAgentException('No response from the LLM.');
      }
      // Need more info.
      if ($response['status'] == 1) {
        return new DirectSolution($response['message'], 'backlog');
      }
      // Can't do.
      if ($response['status'] == 0) {
        return new DirectSolution($response['message'], 'closed');
      }
      // Can do.
      foreach ($response['tasks'] as $taskPrompt) {
        $this->generateTask($taskPrompt);
      }

      return new DirectSolution($response['message'], 'in_progress');
    }
    else {
      // Load the agent.
      /** @var \Drupal\ai_agents\PluginBase\AiAgentEntityWrapper $agent */
      $agent = $this->agentManager->createInstance($orchestration_agent->id());
      if (!$agent) {
        throw new FaultyAgentException('Orchestration agent not found.');
      }
      // Let the agent think, but not solve anything.
      $task = new Task($this->getFullContextOfEpic(FALSE));
      $agent->setTask($task);
      $agent->setAiProvider($provider);
      $agent->setModelName($defaults['model_id']);
      $agent->setAiConfiguration([]);
      $agent->setLooped(FALSE);
      $solvability = $agent->determineSolvability();
      if ($solvability !== AiAgentInterface::JOB_SOLVABLE) {
        return new DirectSolution($agent->answerQuestion(), 'backlog');
      }
      $i = 0;
      $provider->setChatSystemRole('');
      $general_description_context = "";
      foreach ($agent->getData() as $object) {
        // Get a title based on the description.
        $tags[] = 'minikanban_orchestration_title_task';
        $input = new ChatInput([
          new ChatMessage('user', "Look at the following task prompt and come up with a short title for it, less then 10 words or 150 characters. \n\n" . $object->prompt),
        ]);
        $title = $provider->chat($input, $defaults['model_id'], $tags)->getNormalized();
        $label = method_exists($object, 'getAiAgentEntity') ? $object->getAiAgentEntity()->label() : 'Test';
        $title_text = $title->getText();
        // Figure out if images and files.
        $file_message = "I am creating a task based on an epic. In the epic are some files embedded in HTML. Can you figure out based on the task at hand which files needs to be forwarded and list them exactly as they are in the HTML\n";
        $file_message .= "Please answer with a list of files that need to be forwarded, one per row. It should be the full html of the img tag and not just the file src.\n";
        $file_message .= "If no files are needed please answer with 'no'.\n";
        $file_message .= "The task at hand is labeled: " . $title_text . "\n";
        $file_message .= "The task text is:\n";
        $file_message .= $object->prompt . "\n";
        $file_message .= "The context of the epic is:\n";
        $file_message .= $this->getFullContextOfEpic(FALSE);
        $file_input = new ChatInput([
          new ChatMessage('user', $file_message),
        ]);
        $tags[] = 'minikanban_orchestration_file_task';
        $files = $provider->chat($file_input, $defaults['model_id'], $tags)->getNormalized();
        $text = $object->prompt;
        if ($files->getText() != 'no') {
          $text .= "<br /><br />The following files need to be used:<br />";
          $text .= str_replace(['```html', '```'], '', $files->getText());
        }
        $task_prompt = [
          'id' => $i,
          'function_name' => $object->getFunctionName(),
          'description' => $text,
          'title' => substr($title_text, 0, 150),
          'plugin_id' => $object->getFunctionName(),
          'agent_id' => 'ai_agent:' . $object->getFunctionName(),
          'agent_name' => $label,
          'final_result' => ($i == count($agent->getData()) - 1) ? TRUE : FALSE,
          'status' => $i === 0 ? 'todo' : 'blocked',
          'dependencies' => $i === 0 ? [] : [$i - 1],
        ];
        $this->generateTask($task_prompt);
        $general_description_context = $label . ' should do: ' . $object->prompt . "\n";
        $i++;
      }
      // Get the comment back.
      $input = new ChatInput([
        new ChatMessage('user', "Look at the following tasks that are given to different agents and write a comment on how they will be solved. Two sentences maximum. \n\n" . $general_description_context),
      ]);
      $message = $provider->chat($input, $defaults['model_id'], $tags)->getNormalized();
      return new DirectSolution($message->getText(), 'in_progress');
    }
  }

  /**
   * Generate the prompt for the epic to task generation.
   *
   * @return string
   *   The prompt.
   *
   * @throws \Drupal\minikanban_agent\Exceptions\FaultyAgentException
   */
  public function generateEpicToTaskPrompt() {
    $file = $this->extensionPathResolver->getPath('module', 'minikanban_agent') . '/prompts/createTasks.txt';
    $prompt = file_get_contents($file);
    // Get all agents on the system.
    $agents = $this->agentManager->getDefinitions();
    if (empty($agents)) {
      throw new FaultyAgentException('No agents available.');
    }
    $workers = "";
    foreach ($agents as $agent) {
      /** @var \Drupal\ai_agents\PluginInterfaces\AiAgentInterface $current_agent */
      $current_agent = $this->agentManager->createInstance($agent['id']);
      $capabilities = $current_agent->agentsCapabilities();
      foreach ($capabilities as $id => $capability) {
        $workers .= 'Name: ' . $capability['name'] . "\n";
        $workers .= 'Summary: ' . $capability['description'] . "\n";
        $workers .= 'Plugin ID: ' . $current_agent->getId() . "\n";
        $workers .= 'Agent ID: ' . $id . "\n";
        if (isset($capability['inputs'])) {
          $workers .= "Inputs:\n";
          foreach ($capability['inputs'] as $key => $input) {
            $workers .= '  - ' . $key . " (" . $input['type'] . "): " . $input['description'] . "\n";
          }
        }
        if (isset($capability['outputs'])) {
          $workers .= "Outputs:\n";
          foreach ($capability['outputs'] as $key => $output) {
            $workers .= '  - ' . $key . " (" . $output['type'] . "): " . $output['description'] . "\n";
          }
        }
        $workers .= "\n";
      }
    }
    $context = $this->getFullContextOfEpic(FALSE);
    $prompt = str_replace(['{workers}', '{context}'], [$workers, $context], $prompt);
    return $prompt;
  }

  /**
   * Generate task.
   *
   * @param array $taskPrompt
   *   The task prompt from the LLM.
   *
   * @return \Drupal\minikanban\Entity\Task
   *   The task created.
   */
  public function generateTask(array $taskPrompt) {
    // Set blockers.
    $blockers = [];
    foreach ($taskPrompt['dependencies'] as $blocker) {
      if (isset($this->blockersTranslation[$blocker])) {
        $blockers[] = $this->blockersTranslation[$blocker];
      }
    }
    // Create the task.
    $task = $this->entityTypeManager->getStorage('kanban_task')->create([
      'label' => $taskPrompt['title'],
      'description' => [
        'format' => 'kanban_html',
        'value' => $taskPrompt['description'],
      ],
      'epic' => $this->epic->id(),
      'status' => $taskPrompt['status'],
      'assigned' => $this->config->get('main_user'),
      'ai_agent' => $taskPrompt['plugin_id'] . '__' . $taskPrompt['agent_id'],
      'ai_final_result' => $taskPrompt['final_result'] == TRUE || strtolower($taskPrompt['final_result']) == "true" ? 1 : 0,
      'blockers' => $blockers,
      'uid' => $this->epic->getAuthor(),
      'project' => $this->epic->getProject(),
    ]);

    $task->save();
    $this->blockersTranslation[$taskPrompt['id']] = $task->id();
    return $task;
  }

  /**
   * Get full context of the epic.
   *
   * @param bool $stripTags
   *   Strip tags.
   *
   * @return string
   *   The context.
   */
  public function getFullContextOfEpic($stripTags = TRUE) {
    // Get the description and the comments.
    $context = "Epic Title: " . $this->epic->getTitle() . "\n";
    $context .= "Epic Author: " . $this->epic->getAuthorsUsername() . "\n";
    $context .= "Epic Description:\n" . $this->epic->getDescription();
    $context .= "\n--------------------------\n";
    $comments = $this->epic->getComments();
    if (!empty($comments)) {
      $context .= "These are the following comments:\n";
      $context .= "--------------------------\n";
      $comments = $this->epic->getComments();

      $i = 1;
      foreach ($comments as $comment) {
        $context .= "Comment order $i: \n";
        $context .= "Comment Author: " . $comment->getAuthorsUsername() . "\n";
        $context .= "Comment:\n" . $comment->getComment() . "\n\n";
        $i++;
      }
      $context .= "--------------------------\n";
    }
    return $stripTags ? strip_tags($context) : $context;
  }

  /**
   * Cleanup and decode json response.
   *
   * @param string $response
   *   The response.
   *
   * @return array
   *   The response.
   */
  public function cleanupAndDecodeJsonResponse($response) {
    // Remove white lines at start and finish.
    $response = trim($response);
    // Remove json notation.
    $response = str_replace(['```json', '```'], '', $response);
    // Trim again.
    $response = trim($response);
    // Decode the json.
    return json_decode($response, TRUE);
  }

}
