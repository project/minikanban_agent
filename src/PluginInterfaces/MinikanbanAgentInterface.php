<?php

namespace Drupal\minikanban_agent\PluginInterfaces;

use Drupal\minikanban\Entity\Task;

/**
 * Interface for Minikanban Agents modifiers.
 */
interface MinikanbanAgentInterface {

  /**
   * The status of the job.
   */
  // Not solvable.
  const JOB_NOT_SOLVABLE = 0;
  // Solvable.
  const JOB_SOLVABLE = 1;
  // Needs answers.
  const JOB_NEEDS_ANSWERS = 2;
  // Should answer question.
  const JOB_SHOULD_ANSWER_QUESTION = 3;

  /**
   * Gets the plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getId();

  /**
   * Gets the state.
   *
   * @return array
   *   The state.
   */
  public function getState();

  /**
   * Sets the state.
   *
   * @param array $state
   *   The state.
   */
  public function setState(array $state);

  /**
   * Gets the results.
   *
   * @return array
   *   The results.
   */
  public function getResults();

  /**
   * Sets the results.
   *
   * @param array $results
   *   The results.
   */
  public function setResults(array $results);

  /**
   * Set the task.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   */
  public function setTask(Task $task);

  /**
   * Get the task.
   *
   * @return \Drupal\minikanban\Entity\Task
   *   The task.
   */
  public function getTask();

  /**
   * Set the sub agent.
   */
  public function setSubAgent($subAgent);

  /**
   * Get the sub agent.
   */
  public function getSubAgent();

  /**
   * Get sub agent title.
   */
  public function getSubAgentTitle();

  /**
   * Sets the llm.
   *
   * @param \Drupal\minikanban_agent\PluginInterfaces\MinikanbanLlmInterface $llm
   *   The llm.
   */
  public function setLlm(MinikanbanLlmInterface $llm);

  /**
   * Get the list of the available agents names.
   *
   * @return array
   *   The list of the agents names.
   */
  public function agentsNames();

  /**
   * Get the list of the agents name with a description of capabilities.
   *
   * @return array
   *   The agents with a description of capabilities and outputs.
   */
  public function agentsCapabilities();

  /**
   * Checks if the agent is available.
   *
   * @return bool
   *   TRUE if the agent is available, FALSE otherwise.
   */
  public function isAvailable();

  /**
   * Get the amount of retries this agent can do.
   *
   * @return int
   *   The amount of retries this agent can do.
   */
  public function getRetries();

  /**
   * Answers a question from the PM AI or end-users directed at an agent.
   *
   */
  public function answerQuestion();

  /**
   * Get general help about an agent/sub-agent.
   */
  public function getHelp();

  /**
   * Determine if the agent can solve the instructions.
   *
   * @return int
   *   One of the JOB_* constants.
   */
  public function determineSolvability();

  /**
   * Ask a question to the PM AI or end-users to solve the issue.
   *
   * @return array
   *   The question to be asked to the PM AI or end-users.
   */
  public function askQuestion();

  /**
   * Take instructions from the PM AI and solve it.
   *
   * @return \Drupal\minikanban_agent\AgentSolutions\AgentSolutionInterface
   *   The solution from the agent.
   */
  public function solve();

}
