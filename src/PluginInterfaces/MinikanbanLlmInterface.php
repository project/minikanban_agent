<?php

namespace Drupal\minikanban_agent\PluginInterfaces;

/**
 * Interface for LLM Interfaces.
 */
interface MinikanbanLlmInterface {

  /**
   * Prompts the LLM agent to answer a question.
   *
   * @param string $prompt
   *   The prompt to answer.
   * @param array $config
   *   The LLM specific configuration.
   * @param \Drupal\file\Entity\File[] $files
   *   The files to be used in the answer.
   */
  public function prompt($prompt, array $config = [], array $files = []);

}
