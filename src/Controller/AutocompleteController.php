<?php

namespace Drupal\minikanban_agent\Controller;

use Drupal\ai_interpolator_agent\EntityWorkflows;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\minikanban_agent\AgentHelper;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * The agent helper.
   *
   * @var \Drupal\minikanban_agent\AgentHelper
   */
  protected $agentHelper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity workflows.
   *
   * @var \Drupal\ai_interpolator_agent\EntityWorkflows
   */
  protected $entityWorkflows;

  /**
   * {@inheritdoc}
   */
  public function __construct(AgentHelper $agentHelper, EntityTypeManagerInterface $entity_type_manager, EntityWorkflows $entityWorkflows) {
    $this->agentHelper = $agentHelper;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityWorkflows = $entityWorkflows;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('minikanban_agent.agent_helper'),
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_agent.entity_workflows')
    );
  }

  /**
   * Autocomplete any ai agents.
   */
  public function aiAgents(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists and longer than 3 chars.
    if (!$input || strlen($input) < 3) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    // Get all agents.
    //$allAgents = $this->agentHelper->agentsName();
    $found = 0;
    foreach ($allAgents as $key => $name) {
      if (strpos(strtolower($name), strtolower($input)) !== FALSE) {
        $found++;
        $results[] = [
          'value' => $key,
          'label' => $name,
        ];
      }
      if ($found >= 10) {
        break;
      }
    }

    // Clean up agents with key / value.

    return new JsonResponse($results);
  }

}
