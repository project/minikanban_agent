<?php

namespace Drupal\minikanban_agent\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ai_agents\PluginBase\AiAgentEntityWrapper;
use Drupal\ai_agents\PluginManager\AiAgentManager;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route where you can list the swarm and its projects.
 */
class Swarms extends ControllerBase {

  /**
   * The constructor.
   *
   * @var \Drupal\ai_agents\PluginManager\AiAgentManager $agentHelper
   *   The agent plugin manager.
   */
  public function __construct(
    protected AiAgentManager $agentHelper,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('plugin.manager.ai_agents'),
    );
  }

  /**
   * List all the swarms.
   */
  public function swarms(Request $request) {
    $swarms = $this->agentHelper->getDefinitions();
    $content = $this->t("<p>These are the swarms that you can create projects for and assign epics and tasks to.</p>");
    foreach ($swarms as $swarm) {
      /** @var \Drupal\ai_agents\PluginBase\AiAgentEntityWrapper $swarm_instance */
      $swarm_instance = $this->agentHelper->createInstance($swarm['id']);
      // It has to be a config agent.
      if (!($swarm_instance instanceof AiAgentEntityWrapper)) {
        continue;
      }
      $agent_entity = $swarm_instance->getAiAgentEntity();
      if (!$agent_entity->get('orchestration_agent')) {
        continue;
      }
      // @todo create template.
      $content .= '<div class="swarm"><h3>' . $swarm['label'] . '</h3>';
      $content .= '<p class="description">' . $agent_entity->get('description') . '</p>';
      // Create an edit link for the swarm.
      $edit_link = $agent_entity->toLink($this->t('Edit Swarm'), 'edit-form', [
        'attributes' => [
          'class' => ['button', 'btn'],
        ],
        'query' => [
          'destination' => \Drupal::destination()->getAsArray()['destination'] ?? '',
        ],
      ])->toString();
      // Create a link from route.
      $url = Url::fromRoute('minikanban_agent.swarm_projects', [
        'swarm_id' => $swarm['id'],
      ], [
        'attributes' => [
          'class' => ['button', 'btn'],
        ],
      ]);
      // Create a link from route.
      $projects_link = Link::fromTextAndUrl($this->t('View Projects'), $url)->toString();
      $content .= $projects_link;
      $content .= $edit_link;
      $content .= '</div>';
    }
    return [
      '#markup' => $content,
      '#attached' => [
        'library' => [
          'minikanban_agent/swarm',
        ],
      ],
    ];
  }

  /**
   * List all the projects for a swarm.
   */
  public function projects(string $swarm_id) {
    $swarm = $this->agentHelper->getDefinition($swarm_id);
    // Get the projects for this swarm.
    $projects = $this->entityTypeManager()->getStorage('kanban_project')->loadByProperties(['field_orchestration_agent' => $swarm_id]);
    $content = $this->t("<p>These are the projects for the swarm: @swarm</p>", ['@swarm' => $swarm['label']]);
    foreach ($projects as $project) {
      $content .= '<div class="project"><h3>' . $project->label() . '</h3>';
      $content .= $project->description->value;
      // Link to the Kanban board.
      $url = Url::fromRoute('minikanban.kanban_board_project', [
        'project' => $project->id(),
      ], [
        'attributes' => [
          'class' => ['button', 'btn'],
        ],
      ]);
      $board_link = Link::fromTextAndUrl($this->t('View Board'), $url)->toString();
      // Create an edit link for the project.
      $link = $project->toLink($this->t('Edit Project'), 'edit-form', [
        'attributes' => [
          'class' => ['button', 'btn'],
        ],
        'query' => [
          'destination' => \Drupal::destination()->getAsArray()['destination'] ?? '',
        ],
      ])->toString();
      $content .= $board_link;
      $content .= $link;
      $content .= '</div>';
    }
    return [
      '#markup' => $content,
      '#attached' => [
        'library' => [
          'minikanban_agent/swarm',
        ],
      ],
    ];
  }

}
