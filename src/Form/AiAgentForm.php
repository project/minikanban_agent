<?php

namespace Drupal\minikanban_agent\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Configure Ai Agent Form access.
 */
class AiAgentForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'minikanban_agent.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'minikanban_agent_base_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    // Get the uid for the main user.
    $form['main_user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Main User'),
      '#description' => $this->t('This is the FICTIONAL user that will be assigned all the tasks and that you can assign epics to for generation. <strong> This syncs on UID which might be different per environment, so please use settings.php to set this for each</strong>'),
      '#target_type' => 'user',
      '#default_value' => $config->get('main_user') ? User::load($config->get('main_user')) : NULL,
      '#required' => TRUE,
    ];

    $form['fallback_approval_user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Fallback Manual Approval User'),
      '#description' => $this->t('The REAL user that should be used as fallback for approvals where needed. <strong> This syncs on UID which might be different per environment, so please use settings.php to set this for each</strong>'),
      '#target_type' => 'user',
      '#default_value' => $config->get('fallback_approval_user') ? User::load($config->get('fallback_approval_user')) : NULL,
      '#required' => TRUE,
    ];

    $form['fallback_worker_user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Fallback Manual Worker User'),
      '#description' => $this->t('The REAL user that should be used to take over tasks the AI can\'t solve. <strong> This syncs on UID which might be different per environment, so please use settings.php to set this for each</strong>'),
      '#target_type' => 'user',
      '#default_value' => $config->get('fallback_worker_user') ? User::load($config->get('fallback_worker_user')) : NULL,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('main_user', $form_state->getValue('main_user'))
      ->set('fallback_approval_user', $form_state->getValue('fallback_approval_user'))
      ->set('fallback_worker_user', $form_state->getValue('fallback_worker_user'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
